// hbcs-parse.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <fstream>
#include <queue>
#include <cstdio>
#include <cstring>



/* DOW CRC algorithm lookup table with polynomial x^8+x^5+x^4+1 */
const unsigned char CRC8_TABLE[] = {
     0, 94, 188, 226, 97, 63, 221, 131, 194, 156, 126, 32, 163, 253, 31, 65,
    157, 195, 33, 127, 252, 162, 64, 30, 95, 1, 227, 189, 62, 96, 130, 220,
    35, 125, 159, 193, 66, 28, 254, 160, 225, 191, 93, 3, 128, 222, 60, 98,
    190, 224, 2, 92, 223, 129, 99, 61, 124, 34, 192, 158, 29, 67, 161, 255,
    70, 24, 250, 164, 39, 121, 155, 197, 132, 218, 56, 102, 229, 187, 89, 7,
    219, 133, 103, 57, 186, 228, 6, 88, 25, 71, 165, 251, 120, 38, 196, 154,
    101, 59, 217, 135, 4, 90, 184, 230, 167, 249, 27, 69, 198, 152, 122, 36,
    248, 166, 68, 26, 153, 199, 37, 123, 58, 100, 134, 216, 91, 5, 231, 185,
    140, 210, 48, 110, 237, 179, 81, 15, 78, 16, 242, 172, 47, 113, 147, 205,
    17, 79, 173, 243, 112, 46, 204, 146, 211, 141, 111, 49, 178, 236, 14, 80,
    175, 241, 19, 77, 206, 144, 114, 44, 109, 51, 209, 143, 12, 82, 176, 238,
    50, 108, 142, 208, 83, 13, 239, 177, 240, 174, 76, 18, 145, 207, 45, 115,
    202, 148, 118, 40, 171, 245, 23, 73, 8, 86, 180, 234, 105, 55, 213, 139,
    87, 9, 235, 181, 54, 104, 138, 212, 149, 203, 41, 119, 244, 170, 72, 22,
    233, 183, 85, 11, 136, 214, 52, 106, 43, 117, 151, 201, 74, 20, 246, 168,
    116, 42, 200, 150, 21, 75, 169, 247, 182, 232, 10, 84, 215, 137, 107, 53
};

unsigned char crc8(unsigned char* bytes, unsigned char nbytes)
{
    unsigned char crc = 0xFF;
    while (nbytes-- > 0)
    {
        crc = CRC8_TABLE[crc ^ *bytes++];
    }
    return crc;
}


int receiveData(std::fstream &f, std::queue<unsigned char>& fifo) {
    // if more data needed, add to the queue
    // simulate reading from file
    unsigned char b;
    if (fifo.size() < 5) {
        // add more to fifo
        for (auto i = 0; i < 5; i++) {
            f.read((char*)&b, 1);
            if (f.eof()) {
                return -1;
            }
            fifo.push(b);
        }
    }
    return 0;
}

// returns -1 : bad message, 0 : good message, 1: no message, continue
int parseHBCSCommand(std::queue<unsigned char>& fifo, unsigned char msg[], size_t msg_size, size_t &msg_index) {
    unsigned char rcrc;
    unsigned char ccrc;
    int status;
    char line[128];
    while(fifo.size() > 0) {

        // get one element from the queue
        msg[msg_index] = fifo.front();
        fifo.pop();

        // advance the msg index
        msg_index++;

        // check if msg is full
        if (msg_index < msg_size) {
            // not full, keep filling msg
            continue;
        }
        // msg if full, check it 
        // 
        // check the crc
        rcrc = msg[4];
        msg[4] = 0;
        ccrc = crc8(msg,msg_size);
        msg[4] = rcrc;
        if (ccrc == rcrc) {
            sprintf_s(line, "OK: %02x,%02x,%02x,%02x,%02x\n", msg[0], msg[1], msg[2], msg[3], msg[4]);
            std::cout << line;
            msg_index = 0;
            status = 0;
            break;
        }
        else {
            // advance one step to sync
            // shuffle the bytes one place to the left
            sprintf_s(line, "NO: %02x,%02x,%02x,%02x,%02x\n", msg[0], msg[1], msg[2], msg[3], msg[4]);
            std::cout << line;
            msg[0] = msg[1];
            msg[1] = msg[2];
            msg[2] = msg[3];
            msg[3] = msg[4];
            msg_index = 4;
            continue;
        }
    }
    return 1;
}

int main()
{
    std::queue<unsigned char> fifo;
    std::fstream infile("../hbcs.bin", std::ios_base::in | std::ios_base::binary);
    unsigned char msg[5];
    size_t msg_index;
    int okcount;
    int nocount;


    if (!infile) {
        std::cout << "can't open data file\n";
        return 1;
    }

    okcount = 0;
    nocount = 0;
    msg_index = 0;
    for(;;) {
        int status = receiveData(infile, fifo);
        if (status < 0) {
            break;
        }

        if (fifo.size() < sizeof(msg)) {
            continue;
        }

        status = parseHBCSCommand(fifo,msg,5,msg_index);
        if (status == 0) {
            okcount++;
        }
        else if (status == -1) {
            nocount++;
        }
        else {
            // continue
        }
        
    }

    std::cout << "ok " << okcount << "\n";
    std::cout << "no " << nocount << "\n";

    return 0;
}

