// hbcs-serial-logger.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <queue>
#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdio>
#include <conio.h>

double timestamp() {
    LARGE_INTEGER ticks;
    LARGE_INTEGER freq;
    QueryPerformanceFrequency(&freq);
    QueryPerformanceCounter(&ticks);
    return ((double)ticks.QuadPart / (double)freq.QuadPart);
}


/* DOW CRC algorithm lookup table with polynomial x^8+x^5+x^4+1 */
const unsigned char CRC8_TABLE[] = {
     0, 94, 188, 226, 97, 63, 221, 131, 194, 156, 126, 32, 163, 253, 31, 65,
    157, 195, 33, 127, 252, 162, 64, 30, 95, 1, 227, 189, 62, 96, 130, 220,
    35, 125, 159, 193, 66, 28, 254, 160, 225, 191, 93, 3, 128, 222, 60, 98,
    190, 224, 2, 92, 223, 129, 99, 61, 124, 34, 192, 158, 29, 67, 161, 255,
    70, 24, 250, 164, 39, 121, 155, 197, 132, 218, 56, 102, 229, 187, 89, 7,
    219, 133, 103, 57, 186, 228, 6, 88, 25, 71, 165, 251, 120, 38, 196, 154,
    101, 59, 217, 135, 4, 90, 184, 230, 167, 249, 27, 69, 198, 152, 122, 36,
    248, 166, 68, 26, 153, 199, 37, 123, 58, 100, 134, 216, 91, 5, 231, 185,
    140, 210, 48, 110, 237, 179, 81, 15, 78, 16, 242, 172, 47, 113, 147, 205,
    17, 79, 173, 243, 112, 46, 204, 146, 211, 141, 111, 49, 178, 236, 14, 80,
    175, 241, 19, 77, 206, 144, 114, 44, 109, 51, 209, 143, 12, 82, 176, 238,
    50, 108, 142, 208, 83, 13, 239, 177, 240, 174, 76, 18, 145, 207, 45, 115,
    202, 148, 118, 40, 171, 245, 23, 73, 8, 86, 180, 234, 105, 55, 213, 139,
    87, 9, 235, 181, 54, 104, 138, 212, 149, 203, 41, 119, 244, 170, 72, 22,
    233, 183, 85, 11, 136, 214, 52, 106, 43, 117, 151, 201, 74, 20, 246, 168,
    116, 42, 200, 150, 21, 75, 169, 247, 182, 232, 10, 84, 215, 137, 107, 53
};

unsigned char crc8(unsigned char* bytes, unsigned char nbytes)
{
    unsigned char crc = 0xFF;
    while (nbytes-- > 0)
    {
        crc = CRC8_TABLE[crc ^ *bytes++];
    }
    return crc;
}
// returns -1 : bad message, 0 : good message, 1: no message, continue
double t0;

int parseHBCSCommand(std::queue<unsigned char>& fifo, unsigned char msg[], size_t msg_size, size_t& msg_index) {
    unsigned char rcrc;
    unsigned char ccrc;
    int status;
    char line[128];
    status = 1;
    while (fifo.size() > 0) {

        // get one element from the queue
        msg[msg_index] = fifo.front();
        fifo.pop();

        // advance the msg index
        msg_index++;

        // check if msg is full
        if (msg_index < msg_size) {
            // not full, keep filling msg
            continue;
        }
        // msg if full, check it 
        // 
        // check the crc
        rcrc = msg[4];
        msg[4] = 0;
        ccrc = crc8(msg, (unsigned char)msg_size);
        msg[4] = rcrc;
        if (ccrc == rcrc) {
            sprintf_s(line, "%.3f,%02x,%02x,%02x,%02x,%02x\n", timestamp() - t0,  msg[0], msg[1], msg[2], msg[3], msg[4]);
            std::cout << line;
            msg_index = 0;
            status = 0;
            break;
        }
        else {
            // advance one step to sync
            // shuffle the bytes one place to the left
            sprintf_s(line, "0.000,%02x,%02x,%02x,%02x,%02x\n", msg[0], msg[1], msg[2], msg[3], msg[4]);
            std::cout << line;
            msg[0] = msg[1];
            msg[1] = msg[2];
            msg[2] = msg[3];
            msg[3] = msg[4];
            msg_index = 4;
            status = -1;
            break;
        }
    }
    return status;
}

int main()
{
    HANDLE hport;
    DCB dcb;
    COMMTIMEOUTS tmo;
    BOOL status;
    unsigned char data[16];
    DWORD bytes;

    std::queue<unsigned char> fifo;
    unsigned char msg[5];
    size_t msg_index;
    int okcount;
    int nocount;

    t0 = timestamp();

    std::fstream outfile("../hbcs.bin", std::ios_base::out | std::ios_base::binary);

    // open the serial port
    hport = CreateFileA("COM4", GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);
    if (hport == INVALID_HANDLE_VALUE) {
        std::cout << "Can't open port : " << GetLastError() << "\n";
        return 1;
    }

    // set up the port for blocking read
    memset(&dcb, 0, sizeof(dcb));
    dcb.DCBlength = sizeof(dcb);

    status = GetCommState(hport, &dcb);
    if (!status) {
        std::cout << "Can't get port status : " << GetLastError() << "\n";
        return 1;
    }
    dcb.DCBlength = sizeof(DCB);
    dcb.fBinary = TRUE;
    dcb.BaudRate = CBR_19200;
    dcb.ByteSize = 8;
    dcb.Parity = NOPARITY;
    dcb.StopBits = ONESTOPBIT;
    
    status = SetCommState(hport, &dcb);
    if (!status) {
        std::cout << "Can't set port status : " << GetLastError() << "\n";
        return 1;
    }

    status = GetCommTimeouts(hport, &tmo);
    if (!status) {
        std::cout << "Can't get port timeouts : " << GetLastError() << "\n";
        return 1;
    }

    tmo.ReadIntervalTimeout = MAXDWORD;
    tmo.ReadTotalTimeoutConstant = 10000;
    tmo.ReadTotalTimeoutMultiplier = MAXDWORD;
    tmo.WriteTotalTimeoutConstant = 0;
    tmo.WriteTotalTimeoutMultiplier = 0;

    status = SetCommTimeouts(hport, &tmo);
    if (!status) {
        std::cout << "Can't set port timeouts : " << GetLastError() << "\n";
        return 1;
    }


    msg_index = 0;
    okcount = 0;
    nocount = 0;
    while(!_kbhit()) {
        status = ReadFile(hport, data, sizeof(data), &bytes, 0);
        if (!status) {
            std::cout << "can't read from serial port\n";
            return -1;
        }
        if (bytes == 0) {
            Sleep(20);
            continue;
        }

        // add data to the fifo
        for (DWORD i = 0; i < bytes; i++) {
            fifo.push(data[i]);
        }

        if (fifo.size() < sizeof(msg)) {
            continue;
        }

        status = parseHBCSCommand(fifo, msg, 5, msg_index);
        if (status == 0) {
            okcount++;
        }
        else if (status == -1) {
            nocount++;
        }
        else {
            // continue
        }
    }
    outfile.close();
    CloseHandle(hport);
    return 0;
}

